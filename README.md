# Anomaly Detector

## Part I

This project is a monorepo for both the backend and frontend.

### Backend

#### Running the Backend App

1. **Navigate to the `backend` directory:**

   ```bash
   cd backend
   ```

2. **Install dependencies:**

   ```bash
   npm install
   ```

3. **Build the TypeScript code:**

   ```bash
   npm run build
   ```

4. **Start the backend server:**

   ```bash
   npm start
   ```

   The server will start listening on the specified port (port 4000) and be ready to accept incoming connections.

### Frontend

#### Running the Frontend App

1. Navigate to the `frontend` directory:

   ```bash
   cd frontend
   ```

2. Install dependencies:

   ```bash
   npm install
   ```

3. Start the development server:

   ```bash
   npm start
   ```

4. Open your browser and visit [http://localhost:3000](http://localhost:3000) to view the frontend application.

## Part II

### Scalability

1. **Non-blocking Asynchronous Communication**:

   - Utilize non-blocking asynchronous communication patterns to handle a large number of concurrent requests. 

2. **Containerization and Horizontal Scaling**:

   - Utilize containerization technologies such as Docker to package the application and its dependencies into lightweight, portable containers.

   - Implement horizontal scaling by running multiple instances of the containerized application across multiple servers or cloud instances.

3. **Cloud Provider**:
   - Consider leveraging a cloud provider's infrastructure (e.g., AWS, Google Cloud Platform) to dynamically scale resources based on demand, allowing for elasticity and cost-effectiveness.

4. **Load Balancing**:

   - Implement load balancing techniques to evenly distribute incoming traffic across multiple instances of the backend application, ensuring optimal resource utilization and scalability.

### Persistence Layer

1. **Time-series Database**:

   - Utilize a time-series database (e.g., InfluxDB, TimescaleDB) optimized for storing and querying time-based data, providing efficient storage and retrieval of historical device readings.

2. **Redis for Caching**:
   - Implement Redis as a caching layer to store frequently accessed data and optimize database queries, reducing latency and improving overall system performance.

### Fault Tolerance

1. **Fault-tolerant Design**:

   - Design the system with fault tolerance in mind, avoiding single points of failure by implementing redundant components and distributed architectures.

2. **Recovery Strategies**:

   - Implement recovery strategies such as retry mechanisms and circuit breakers to handle transient failures and ensure system resilience.

3. **Continuous Integration and Deployment (CI/CD)**:
   - Set up CI/CD pipelines to automate the testing, deployment, and monitoring processes, enabling rapid feedback loops and quick recovery from failures.

### Monitoring

1. **Monitoring Tools**:

   - Utilize monitoring tools such as Prometheus, Grafana to track system performance metrics, identify bottlenecks, and troubleshoot issues in real-time.

2. **Enhanced Logging**:
   - Enhance logging capabilities to capture detailed information about system events, errors, and anomalies, facilitating post-mortem analysis and root cause identification.

### Test Setup

1. **Comprehensive Testing**:

   - Establish a comprehensive testing strategy encompassing unit tests, integration tests, and end-to-end tests to validate the functionality and performance of the application.

2. **Contract Testing**:
   - Implement contract testing within a microservices environment to ensure compatibility and interoperability between services, enabling seamless integration and deployment.
