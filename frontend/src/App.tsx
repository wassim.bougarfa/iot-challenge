import AnomaliesCounter from './AnomaliesCounter/AnomaliesCounter';
import EventFeed from './EventFeed/EventFeed';
import React from 'react';

function App() {
  return (
    <section>
      <div className="container px-6 mx-auto">
        <div className="grid grid-cols-4 gap-6 md:grid-cols-8 lg:grid-cols-12">
          <div className="col-span-6 lg:col-span-9">
            <div className="h-svh overflow-auto"> 
              <EventFeed />
            </div>
          </div>
          <div className="col-span-4 lg:col-span-3">
            <AnomaliesCounter />
          </div>
        </div>
      </div>
    </section>
  );
}

export default App;
