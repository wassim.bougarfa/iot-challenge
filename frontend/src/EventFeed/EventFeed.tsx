import React, { useEffect, useState } from "react";

import { getSocket } from "../services/socket";

export interface DeviceEvent {
  deviceId: string;
  date: string;
  message: string;
}

export default function EventFeed() {
  const [isConnected, setIsConnected] = useState(getSocket().connected);
  const [events, setEvents] = useState<DeviceEvent[]>([]);

  useEffect(() => {
    const socket = getSocket();
    function onConnect() {
      setIsConnected(true);
    }

    function onDisconnect() {
      setIsConnected(false);
    }

    function onDataEvent(value: DeviceEvent) {
      setEvents((prev) => [value, ...prev]);
    }

    socket.on("connect", onConnect);
    socket.on("disconnect", onDisconnect);
    socket.on("deviceEvent", onDataEvent);

    return () => {
      socket.off("connect", onConnect);
      socket.off("disconnect", onDisconnect);
    };
  }, []);

  return (
    <>
      Connection established: {isConnected ? "Yes" : "No"}
      <ul
        aria-label="Changelog feed"
        role="feed"
        className="relative flex flex-col gap-12 py-12 pl-6 before:absolute before:top-0 before:left-6 before:h-full before:-translate-x-1/2 before:border before:border-dashed before:border-slate-200 after:absolute after:top-6 after:left-6 after:bottom-6 after:-translate-x-1/2 after:border after:border-slate-200 "
      >
        {events.map(({ deviceId, message, date }) => (
          <li
            key={deviceId + Math.random()}
            role="article"
            className="relative pl-6 before:absolute before:left-0 before:top-2 before:z-10 before:h-2 before:w-2 before:-translate-x-1/2 before:rounded-full before:bg-emerald-500 before:ring-2 before:ring-white"
          >
            <div className="flex flex-col flex-1 gap-4">
              <h4 className="text-lg font-medium text-emerald-500">
                {deviceId}
                <span className="text-lg font-normal text-slate-500">
                  - {new Date(date).toUTCString()}
                </span>
              </h4>
              <p className=" text-slate-500">{message}</p>
            </div>
          </li>
        ))}
      </ul>
    </>
  );
}
