import { Socket } from "socket.io-client";
import io from "socket.io-client";

let _socket: Socket;
export const getSocket = () => {
    if (!_socket) {
        _socket = io("http://localhost:4000");
    }

    return _socket;
}

