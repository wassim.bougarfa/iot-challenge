import React, { useEffect, useState } from "react";

import { getSocket } from "../services/socket";

export default function AnomaliesCounter() {
  const [anomaliesCount, setAnomaliesCount] = useState<Record<string, number>>(
    {}
  );

  function onAnomaliesCount(value: Record<string, number>) {
    setAnomaliesCount(value);
  }

  useEffect(() => {
    const socket = getSocket();
    socket.on("anomaliesCount", onAnomaliesCount);
  }, []);

  return (
    <>
      <h2>Anomalies Count:</h2>
      {Object.entries(anomaliesCount).map(([deviceId, count]) => (
        <li key={deviceId}>
          {deviceId}: {count}
        </li>
      ))}
    </>
  );
}
