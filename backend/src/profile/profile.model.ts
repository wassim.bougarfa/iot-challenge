export interface DeviceProfile {
    type: string;
    thresholds: {
        upper: number;
        lower: number;
    }
    window: number;
}


export type DeviceProfileMap = { [deviceId: string]: DeviceProfile[] }