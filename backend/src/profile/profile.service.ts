import { DeviceProfile, DeviceProfileMap } from "./profile.model";

import { DeviceDataType } from "../device/device.model";
import profilesDataset from "../../dataset/profiles.json"

export function getProfileByType(deviceId: string, type: DeviceDataType): DeviceProfile {
    const profile = (profilesDataset as DeviceProfileMap)[deviceId].find(p => p.type === type)
    if (!profile) {
        throw `Cannot find device profile ${profile}`;
    }

    return profile;
}

export function getDevices(): string[] {
    return Object.keys(profilesDataset);
}