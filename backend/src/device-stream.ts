import { DeviceData, DeviceDataType } from "./device/device.model";
import { Observable, interval } from "rxjs";

import { map } from "rxjs/operators";

function randomizeType(): DeviceDataType {
    const types = Object.values(DeviceDataType);
    const randomIndex = Math.floor(Math.random() * types.length);
    return types[randomIndex];
}

function randomizeValue(): number {
    return Math.random() * 100;
}

function generateSensorReading(deviceId: string): DeviceData {
    return {
        deviceId,
        data: {
            type: randomizeType(),
            value: randomizeValue(),
        }
    };
}

// Mimic an actual data stream, by generating random values
export function stream(deviceId: string): Observable<DeviceData> {
    const randomInterval = Math.random() * 3000 + 1000;
    return interval(randomInterval).pipe(
        map(() => generateSensorReading(deviceId))
    );
}