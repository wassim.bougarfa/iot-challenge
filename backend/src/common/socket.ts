import { Application } from 'express';
import { Server } from 'socket.io';
import http from 'http';

let server: http.Server;
let io: Server;

function initSocket(app: Application): void {
    server = http.createServer(app);
    io = new Server(server, {
        cors: { origin: "http://localhost:3000", methods: ["GET", "POST"] },
    });

    server.listen(4000, () => { console.log("listening on *:4000"); });
}

export { initSocket, server, io };