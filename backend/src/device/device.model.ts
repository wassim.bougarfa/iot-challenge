export enum DeviceDataType {
    LOAD = 'load',
    TEMP = 'temperature'
}

export interface DeviceData {
    deviceId: string;
    data: {
        type: DeviceDataType;
        value: number;
    }
}