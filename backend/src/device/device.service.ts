import { getDevices, getProfileByType } from "../profile/profile.service";

import { DeviceDataType } from "./device.model";
import { interval } from "rxjs";
import { io } from "../common/socket";
import { stream } from "../device-stream";

let anomalies: Record<string, number> = {};
export interface DeviceEvent {
    deviceId: string;
    date: Date;
    message: string;
}


export function processDataStream(): void {
    const devices = getDevices();

    // Subscribe to each device's data stream
    devices.forEach(deviceId => {
        stream(deviceId)
            .subscribe(({ data: { type, value } }) => processDeviceData(deviceId, type, value))
    });

    // Emit results periodically
    interval(10000) // Every 10 seconds
        .subscribe(() => emitAnomaliesCount());
}

export function processDeviceData(deviceId: string, type: DeviceDataType, value: number) {
    console.debug('Processing data for', { deviceId, type, value })
    const { thresholds: { upper, lower } } = getProfileByType(deviceId, type)

    // Check if value is above or below thresholds
    const isAnomaly = value > upper || value < lower;
    if (isAnomaly) {
        console.log(`Found anomaly from device "${deviceId}", "${type}" should be between ${lower} and ${upper}, got ${value} instead`)
        // Check if this is the first event for this device
        if (!anomalies[deviceId]) {
            anomalies[deviceId] = 1;
        } else {
            anomalies[deviceId]++;
        }

    }

    // emit socket.io event
    io.emit('deviceEvent',
        {
            deviceId,
            date: new Date(),
            message: isAnomaly ?
                `Anomaly detected, "${type}" should be between ${lower} and ${upper}, got ${value} instead`
                : `Device ${type} is ${value}`
        }
    )
}

export function emitAnomaliesCount() {
    // Sort events map by number of events
    const sortedEvents = Object.entries(anomalies)
        .sort((a, b) => b[1] - a[1])
        .map(([deviceId, count]) => `${deviceId},${count}`);

    console.log("Anomalies:", sortedEvents);


    io.emit('anomaliesCount', anomalies);
    // clear anomalies
    // anomalies = {};
}