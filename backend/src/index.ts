import { initSocket, io } from './common/socket';

import cors from 'cors';
import express from 'express';
import { processDataStream } from "./device/device.service";

// Start processing the data stream
processDataStream();

const app = express();

app.use(cors())

initSocket(app)
